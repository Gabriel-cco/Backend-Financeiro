package com.picpaysimplificado.dtos;

public record NotificationDTO(String message, String email) {
}
