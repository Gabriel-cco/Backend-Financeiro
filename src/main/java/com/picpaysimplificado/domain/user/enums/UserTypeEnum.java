package com.picpaysimplificado.domain.user.enums;

public enum UserTypeEnum {
    COMMON,
    MERCHANT;
}
